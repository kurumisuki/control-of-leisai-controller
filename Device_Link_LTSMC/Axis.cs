namespace Device_Link_LTSMC
{
	public enum Axis : ushort
	{
		X,
		Y,
		Z,
		U,
		V,
		W
	}
}
