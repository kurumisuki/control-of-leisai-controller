namespace Device_Link_LTSMC
{
	/// <summary>
	/// 轴异常上报事件
	/// </summary>
	/// <param name="erroCode"></param>
	/// <param name="erroType"></param>
	/// <param name="erroTip"></param>
	/// <param name="axis"></param>
	/// <param name="optionFunction"></param>
	public delegate void AxisErroCallBack(short[] erroCode, ErroType erroType, string erroTip, Axis axis, string optionFunction);
}
