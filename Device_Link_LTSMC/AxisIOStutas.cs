using System;

namespace Device_Link_LTSMC
{
	[Flags]
	public enum AxisIOStutas : uint
	{
		Defult = 0x0u,
		ALM = 0x1u,
		ELZ = 0x2u,
		ELF = 0x4u,
		EMG = 0x8u,
		ORG = 0x10u,
		SLZ = 0x20u,
		SLF = 0x40u,
		INP = 0x80u,
		EZ = 0x100u,
		RDY = 0x200u,
		DSTP = 0x400u
	}
}
