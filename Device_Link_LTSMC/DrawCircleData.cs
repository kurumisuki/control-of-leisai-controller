using System;
using System.Collections.Generic;

namespace Device_Link_LTSMC
{
	public class DrawCircleData
	{
		private List<double> _horPosition = new List<double>();

		private List<double> _horVel = new List<double>();

		private List<double> _horTime = new List<double>();

		private List<double> _vecPosition = new List<double>();

		private List<double> _vecVel = new List<double>();

		private List<double> _vecTime = new List<double>();

		private int _accurary = 10;

		private double _time = 5.0;

		private double _longR = 5000.0;

		private double _shortR = 5000.0;

		private double _angle = 360.0;

		private Direction _dir = Direction.Forward;

		private Axis _horAxis = Axis.X;

		private Axis _vecAxis = Axis.Y;

		public Direction Dir
		{
			get
			{
				return _dir;
			}
			set
			{
				_dir = value;
			}
		}

		public Axis VecAxis
		{
			get
			{
				return _vecAxis;
			}
			set
			{
				_vecAxis = value;
			}
		}

		internal double[] ToHorPositionArray => _horPosition.ToArray();

		internal double[] ToHorVelArray => _horVel.ToArray();

		internal double[] ToHorTimeArray => _horTime.ToArray();

		internal double[] ToVecPositionArray => _vecPosition.ToArray();

		internal double[] ToVecVelArray => _vecVel.ToArray();

		internal double[] ToVecTimeArray => _vecTime.ToArray();

		public Axis HorAxis
		{
			get
			{
				return _horAxis;
			}
			set
			{
				_horAxis = value;
			}
		}

		public double Time
		{
			get
			{
				return _time;
			}
			set
			{
				_time = value;
			}
		}

		public int Length => _horTime.Count;

		public DrawCircleData(double time, double longR, double shortR, double angle, int accurary)
		{
			_longR = longR;
			_shortR = shortR;
			_angle = angle;
			_time = time;
			_accurary = accurary;
		}

		internal void GetDrawCircData()
		{
			double everyAngle = 90.0 / (double)_accurary;
			_horPosition.Clear();
			_horTime.Clear();
			_horVel.Clear();
			_vecPosition.Clear();
			_vecTime.Clear();
			_vecVel.Clear();
			double num = Time / (double)(_accurary * 4);
			_horTime.Add(0.0);
			_vecTime.Add(0.0);
			for (int i = 1; i <= _accurary * 4; i++)
			{
				_horTime.Add((double)i * num);
				_vecTime.Add((double)i * num);
			}
			if (Dir == Direction.Forward)
			{
				GetDrawCircleAccuracyData(180.0, everyAngle);
				GetDrawCircleAccuracyData(90.0, everyAngle);
				GetDrawCircleAccuracyData(0.0, everyAngle);
				GetDrawCircleAccuracyData(270.0, everyAngle, isLast: true);
			}
			else
			{
				GetDrawCircleAccuracyData(180.0, everyAngle);
				GetDrawCircleAccuracyData(270.0, everyAngle);
				GetDrawCircleAccuracyData(0.0, everyAngle);
				GetDrawCircleAccuracyData(90.0, everyAngle, isLast: true);
			}
		}

		private void GetDrawCircleAccuracyData(double startAngle, double everyAngle, bool isLast = false)
		{
			double num = Math.PI / 180.0;
			double num2 = Math.PI * -2.0 / Time;
			double num3 = 0.0;
			int num4 = (isLast ? (_accurary + 1) : _accurary);
			for (int i = 0; i < num4; i++)
			{
				num3 = num * (startAngle - (double)i * everyAngle);
				_horPosition.Add(_longR * Math.Cos(num3) + _longR);
				_horVel.Add((0.0 - _longR) * Math.Sin(num3) * num2);
				_vecPosition.Add(_shortR * Math.Sin(num3) * 4.0);
				_vecVel.Add(_shortR * Math.Cos(num3) * num2 * 4.0);
			}
		}
	}
}
