namespace Device_Link_LTSMC
{
	public static class ErroInfoClass
	{
		public static string Success = "操作成功";

		public static string Unknown_Error = "未知错误";

		public static string Parameter_Error = "参数错误";

		public static string Operate_Timeout = "操作超时";

		public static string State_Busy = "状态忙";

		public static string Too_Many_Connections = "链接太多";

		public static string Interpolation_Error = "插补错误";

		public static string Connection_Failure = "连接失败";

		public static string Cannot_be_connected = "无法连接";

		public static string Axis_number_is_out_of_range = "卡号或轴号超出范围";

		public static string transport_error = "数据传输错误";

		public static string firmware_file_error = "固件文件错误";

		public static string The_firmware_does_not_match = "固件文件不匹配";

		public static string firmware_parameters_error = "固件参数错误";

		public static string The_current_state_of_firmware_is_not_allowed_to_operate = "固件当前状态不允许操作";

		public static string The_feature_is_not_supported = "不支持该功能";

		public static string password_error = "密码错误";

		public static string The_number_of_password_inputs_is_limited = "密码输入次数受限";

		public static string ERR_AXIS_SEL_ERR = "手轮脉冲的轴档位选择超出范围（软件控制模式）";

		public static string ERR_HAND_AXIS_NUM_ERR = "手轮脉冲的轴映射数量超出范围";

		public static string ERR_AXIS_RATIO_ERR = "手轮脉冲的倍率档位选择超出范围";

		public static string ERR_HANDWH_START = "已进入手轮脉冲模式，不能切换软硬件控制模式";

		public static string ERR_AXIS_BUSY_STATE = "轴已在运动，不能切换到手轮模式";

		public static string ERR_LIST_NUM_ERR = "LIST号超出范围";

		public static string ERR_LIST_NOT_OEPN = "LIST没有初始化";

		public static string ERR_PARA_NOT_VALID = "参数不在有效范围";

		public static string ERR_LIST_HAS_OPEN = "LIST已经打开";

		public static string ERR_MAIN_LIST_NOT_OPEN = "LIST没有初始化";

		public static string ERR_AXIS_NUM_ERR = "轴数不在有效范围";

		public static string ERR_AXIS_MAP_ARRAY_ERR = "轴映射表为空";

		public static string ERR_MAP_AXIS_ERR = "映射轴错误";

		public static string ERR_MAP_AXIS_BUSY = "映射轴忙";

		public static string ERR_PARA_SET_FORBIT = "运动中不允许更改参数";

		public static string ERR_FIFO_FULL = "缓冲区已满";

		public static string ERR_RADIUS_ERR = "半径为0或小于两点的距离的一半";

		public static string ERR_MAINLIST_HAS_START = "LIST已经启动";

		public static string ERR_ACC_TIME_ZERO = "加减速时间为0";

		public static string ERR_MAINLIST_NOT_START = "主要LIST没有启动";

		public static string ERR_POINT_SAME_ON_RADIUS = "圆弧插补在半径模式下起点和终点不能重合";

		public static string MCVP_SMOOTH_TIME_SET_ERROR = "s时间设置错误(小于等于0)";

		public static string MCVP_START_VEL_SET_ERROR = "起始速度绝对值设置错误(小于0)";

		public static string MCVP_STEADY_VEL_SET_ERROR = "最大速度绝对值设置错误(小于等于0)";

		public static string MCVP_END_VEL_SET_ERROR = "停止速度绝对值设置错误(小于0)";

		public static string MCVP_TOTAL_LENGTH_SET_ERROR = "运动距离为0，无法运动";

		public static string ERR_AXIS_INDEX = "所选轴超出最大值";

		public static string ERR_SET_WHILE_MOVING = "轴正在运动，不能设置参数";

		public static string ERR_ENTER_WHILE_MOVING = "轴正在运动，不能进入该模式";

		public static string ERR_PEL_STATE = "轴处于正限位，不能正向运动";

		public static string ERR_NEL_STATE = "轴处于负限位，不能负向运动";

		public static string ERR_SOFT_PEL_STATE = "轴处于软正限位，不能正向运动";

		public static string ERR_SOFT_NEL_STATE = "轴处于软负限位，不能负向运动";

		public static string ERR_FORCE_IN_OTHER_MODE = "轴处于非点位模式，不能强制变位";

		public static string ERR_MAX_VEL_ZERO = "设置最大速度错误，不能为0";

		public static string ERR_EQU_ZERO = "设置轴当量错误，不能为0";

		public static string ERR_BACKLASH_NEG = "设置反向间隙错误，不能为负值";

		public static string ERR_MAX_PULSE = "设置位置错误，已超出允许范围";

		public static string ERR_CMP_EXCEED_MAX_AXISES = "所选比较轴超出范围";

		public static string ERR_CMP_EXCEED_MAX_INDEX = "比较点数已满，不能继续添加";

		public static string ERR_CMP_EXCEED_MAX_IO = "进行比较的IO超出范围";

		public static string ERR_CMP_EXCEED_MAX_CHAN = "选择的高速比较IO超出范围";

		public static string ERR_MAP_AXISIO_MAX_AXISES = "映射的轴超出范围";

		public static string PVT_ERROR_AXISES_OVER_RANGE = "所选轴超出范围";

		public static string PVT_ERROR_INDEX_OVER_RANGE = "控制点已满，不能继续添加";

		public static string PVT_ERROR_INDEX_EXCEED = "控制点已满，不能继续添加";

		public static string PVT_ERROR_TIME_EROOR = "插入段时间为0或者负数";

		public static string HOME_ERROR_AXISES_OVER_RANGE = "所选轴超出最大值";

		public static string HOME_ERROR_MAX_VEL = "设置的最大速度为0";

		public static string HOME_ERROR_MAX_ACC = "设置的加速度小于等于0";

		public static string HOME_ERROR_BOTH_LIMIT = "同时处于正、负限位,无法启动回零运动";

		public static string ERROR_ZSHELL_STOP_USER = "用户手动停止";

		public static string ERROR_ZSHELL_STOP_OTHERTASK = "其他任务牵连停止";

		public static string ERROR_ZSHELL_PARA_CANNOT_MODIFIY = "少数参数不让修改, SET扩展返回";

		public static string ERROR_ZSHELL_ARRAY_OVER = "数组越界";

		public static string ERROR_ZSHELL_VAR_TOOMUCH = "变量个数超过";

		public static string ERROR_ZSHELL_ARRAY_TOOMUCH = "数组个数超过";

		public static string ERROR_ZSHELL_ARRAY_NOSPACE = "数组没有空间";

		public static string ERROR_ZSHELL_SUB_TOOMUCH = "SUB过多";

		public static string ERROR_ZSHELL_LABEL_NAMEERR = "标识符命名错误";

		public static string ERROR_ZSHELL_LABEL_TOOMANYCHARES = "标识符命名过长";

		public static string ERROR_ZSHELL_NO_RIGHTBRACKET = "没有右括号";

		public static string ERROR_ZSHELL_UNKOWNCHAR = "不认识的字符";

		public static string ERROR_ZSHELL_UNKOWN_LABEL = "不认识的名称, 表达式中碰到";

		public static string ERROR_ZSHELL_STOP_INVALIDCMD = "不认识的命令";

		public static string ERROR_ZSHELL_STOP_OVERSTACK = "超过堆栈层数";

		public static string ERROR_ZSHELL_OVER_RECURSION = "递归过多";

		public static string ERROR_ZSHELL_NO_QUOTEEND = "引号没有结束";

		public static string ERROR_ZSHELL_CMD_CANNOTREAD = "不能读取, 不能在表达式中使用";

		public static string ERROR_ZSHELL_CMD_CANNOTRUN = "函数之类不能出现在行首, 只能在表达式中使用";

		public static string ERROR_ZSHELL_LINE_MUST_END = "期望行结束, 一些特殊指令需要";

		public static string ERROR_ZSHELL_ARRAY_NEEDINDEX = "数组需要编号，参数也使用";

		public static string ERROR_ZSHELL_NOTBRACKET_AFTERVAR = "变量后不需要编号";

		public static string ERROR_ZSHELL_DIM_CONFLICT = "数组重新定义冲突, 长度不一致";

		public static string ERROR_ZSHELL_DIM_ARRAYLENGTHERR = "数组长度错误";

		public static string ERROR_ZSHELL_DIM_LABEL_SUB = "定义, 名称 SUB";

		public static string ERROR_ZSHELL_DIM_LABEL_PARA = "定义, 名称 PARA";

		public static string ERROR_ZSHELL_DIM_LABEL_RESERVE = "定义, 名称保留";

		public static string ERROR_ZSHELL_UNWANT_CHAR = "不能出现的字符";

		public static string ERROR_ZSHELL_STACK_NOPUSH = "POP时没有压栈";

		public static string ERROR_ZSHELL_FORMAT_ERR = "格式错误";

		public static string ERROR_ZSHELL_SET_OVER = "参数溢出, para(10) 10过大";

		public static string ERROR_ZSHELL_PARA_RANGEERR = "一些函数和命令的参数范围错误";

		public static string ERROR_ZSHELL_PARA_TOOMANY = "一些函数和命令的参数过多";

		public static string ERROR_ZSHELL_PARA_TOOFEW = "一些函数和命令的参数过少";

		public static string ERROR_ZSHELL_NO_EXPR = "读取不到表达式";

		public static string ERROR_ZSHELL_OPERNOPARA = "操作符没有参数";

		public static string ERROR_ZSHELL_NOPARA_BEFOREOPER = "操作符前面没有参数";

		public static string ERROR_ZSHELL_SIGNAL_CANNOTINEXPR = "符号不能在表达式中使用";

		public static string ERROR_ZSHELL_NEED_OPER = "需要双目操作符";

		public static string ERROR_ZSHELL_SUB_NOTSUB = "CALL 的不是SUB";

		public static string ERROR_ZSHELL_NO_AUTO = "没有AUTO所以不启动";

		public static string ERROR_ZSHELL_EQ_WANTED = "赋值语句没有等号, 变量或者参数等只能为赋值语句";

		public static string ERROR_ZSHELL_FILE_VAIN = "程序文件空";

		public static string ERROR_ZSHELL_SUB_RENAME = "SUB重名, 包括与其他的名称重了";

		public static string ERROR_ZSHELL_TASK_RUNNING = "任务已经运行中";

		public static string ERROR_ZSHELL_NEED_COMMA_BETWEEN_PARA = "操作数之间需要逗号";

		public static string ERROR_ZSHELL_NO_LEFTBRACKET = "没有右括号";

		public static string ERROR_ZSHELL_TOOMANY_IFNESTED = "IF嵌套太多";

		public static string ERROR_ZSHELL_TOOMANY_LOOPNESTED = "LOOP嵌套太多";

		public static string ERROR_ZSHELL_MOVEAXISES_FEW = "插补轴太少";

		public static string ERROR_ZSHELL_CONST_VAR = "变量不能修改";

		public static string ERROR_ZSHELL_NOT_ONLINECMD = "不能作为在线命令";

		public static string ERROR_ZSHELL_AXIS_OVER = "轴号超出";

		public static string ERROR_ZSHELL_CRD_OVER = "插补系超出";

		public static string ERROR_ZSHELL_STOP_UNKNOWN = "未知错误, 不可能出现的，一般是内部错误引起";
	}
}
