using System;

namespace Device_Link_LTSMC
{
	public class LSTMCException : Exception
	{
		public Axis axis = Axis.X;

		public int ErroCode = 0;

		public string ErroMessage = "";

		public LSTMCException(string msg)
			: base(msg)
		{
		}
	}
}
