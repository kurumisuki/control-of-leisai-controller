using System.Collections;
using System.Collections.Generic;
using System.Linq;

namespace Device_Link_LTSMC
{
	/// <summary>
	/// 单段线性插补
	/// </summary>
	public struct LineInterData
	{
		/// <summary>
		/// 最大运行速度
		/// </summary>
		public double MaxSpeed { get; set; }
		/// <summary>
		/// 加速时间
		/// </summary>
		public double AccTime { get; set; }
		/// <summary>
		/// 减速时间
		/// </summary>
		public double DecTime { get; set; }
		/// <summary>
		/// S段 平滑曲线时间
		/// </summary>
		public double STime { get; set; }
		/// <summary>
		/// 是否使用绝对坐标方式
		/// </summary>
		public bool IsAbsCoord { get; set; }
		/// <summary>
		/// 轴数据
		/// </summary>
		public Dictionary<Axis, double> AxisDatas { get; set; }

		public LineInterData(double maxSpeed, double accTime, double decTime, double sTime, Dictionary<Axis, double> axisDatas, bool isAbsCoord = true)
		{
			this.MaxSpeed = maxSpeed;
			this.AccTime = accTime;
			this.DecTime = decTime;
			this.STime = sTime;
			this.IsAbsCoord = isAbsCoord;
			this.AxisDatas = axisDatas;
		}
	}
	/// <summary>
	/// 连续线性插补数据
	/// </summary>
	public struct ContinueLineInterData
	{
		/// <summary>
		/// 最大运行速度
		/// </summary>
		public double MaxSpeed { get; set; }
		/// <summary>
		/// 加速时间
		/// </summary>
		public double AccTime { get; set; }
		/// <summary>
		/// 减速时间
		/// </summary>
		public double DecTime { get; set; }
		/// <summary>
		/// S段 平滑曲线时间
		/// </summary>
		public double STime { get; set; }
		/// <summary>
		/// 是否使用绝对坐标方式
		/// </summary>
		public bool IsAbsCoord { get; set; }
		/// <summary>
		///  mode >= 0 mode <= 2
		/// </summary>
		public ushort Mode { get; set; }
		/// <summary>
		/// 拐弯加速度
		/// </summary>
		public ushort AccSpeed { get; set; }
		/// <summary>
		/// 轴数据
		/// </summary>
		public Dictionary<Axis, List<double>> AxisDatas { get; set; }


		public ContinueLineInterData(double maxSpeed, double accTime, double decTime, double sTime, ushort mode, ushort accSpeed,
			Dictionary<Axis, List<double>> axisDatas = null, bool isAbsCoord = true)
		{
			this.MaxSpeed = maxSpeed;
			this.AccTime = accTime;
			this.DecTime = decTime;
			this.STime = sTime;
			this.IsAbsCoord = isAbsCoord;
			this.Mode = mode;
			this.AccSpeed = accSpeed;
			if(axisDatas == null)
            {
				AxisDatas = new Dictionary<Axis, List<double>>();
            }
			else
				AxisDatas = axisDatas;
		}

		public void Add(Axis axis, double value)
		{
            if (AxisDatas.ContainsKey(axis))
            {
				AxisDatas[axis].Add(value);
            }
            else
            {
				AxisDatas.Add(axis, new List<double>() { value});
            }
		}

		/// <summary>
		/// 转换到规划
		/// </summary>
		/// <param name="totalTime">总时间</param>
		/// <param name="speedLineScale">速度递增比例</param>
		/// <returns></returns>
		public Dictionary<Axis, SingleAxisAnySpeedTaskData> ToTasks(double totalTime)
        {

			Dictionary<Axis, SingleAxisAnySpeedTaskData> datas = new Dictionary<Axis, SingleAxisAnySpeedTaskData>();
			var axises = AxisDatas.Keys.ToList();

			double timeSpan = totalTime / (double)AxisDatas[axises.First()].Count();

			foreach (var axise in axises)
            {
				var data = new SingleAxisAnySpeedTaskData(SingleSpeedTaskMode.PVT);
				data.IsBack = false;
				int index = 0;
				double forntPos = 0;
                foreach (var item in AxisDatas[axise])
                {
					var speed = (item - forntPos) / timeSpan;
					data.AddPVT(item, timeSpan * index ++, speed);
					forntPos = item;
				}
				datas.Add(axise, data);
            }
			return datas;
		}
	}
}
