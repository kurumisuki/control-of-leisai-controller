using System.Collections.Generic;

namespace Device_Link_LTSMC
{
	public class SingleAxisAnySpeedTaskData
	{
		private static SingleAxisAnySpeedTaskData _emptyData = new SingleAxisAnySpeedTaskData();

		private List<double> _position = new List<double>();

		private List<double> _time = new List<double>();

		private List<double> _percent = new List<double>();

		private List<double> _vel = new List<double>();

		private bool _isBack = false;

		private SingleSpeedTaskMode _mode = SingleSpeedTaskMode.PTT;

		public static SingleAxisAnySpeedTaskData Empty => _emptyData;

		public bool IsReady
		{
			get
			{
				if (Mode == SingleSpeedTaskMode.PTT)
				{
					return Position.Count == Time.Count && Position.Count != 0;
				}
				return Position.Count == Time.Count && Time.Count == Position.Count && Position.Count != 0;
			}
		}

		public double[] PositionArray => Position.ToArray();

		public double[] TimeArray => Time.ToArray();

		public double[] PrecentArray => Percent.ToArray();

		public double[] VelArray => Vel.ToArray();

		public int Count => Position.Count;

		public SingleSpeedTaskMode Mode
		{
			get
			{
				return _mode;
			}
			set
			{
				_mode = value;
			}
		}

		public List<double> Position => _position;

		public List<double> Time => _time;

		public List<double> Percent => _percent;

		public List<double> Vel => _vel;

		public bool IsBack
		{
			get
			{
				return _isBack;
			}
			set
			{
				_isBack = value;
			}
		}

		public SingleAxisAnySpeedTaskData(SingleSpeedTaskMode Mode = SingleSpeedTaskMode.PTT)
		{
			this.Mode = Mode;
		}

		public void AddPTS(double pos, double time, double precent)
		{
			Position.Add(pos);
			Time.Add(time);
			Percent.Add(precent);
		}

		public void AddPTT(double pos, double time)
		{
			Position.Add(pos);
			Time.Add(time);
		}

		public void AddPVT(double pos, double time, double vel)
		{
			Position.Add(pos);
			Time.Add(time);
			Vel.Add(vel);
		}

		public void Clear()
		{
			Position.Clear();
			Time.Clear();
			Percent.Clear();
		}

		internal void SetAutoBack()
		{
			if (Position.Count != 0 && Position[0] != Position[Position.Count - 1])
			{
				List<double> listClone = GetListClone(Position);
				Position.AddRange(listClone);
				List<double> list = new List<double>();
				for (int i = 0; i < Time.Count; i++)
				{
					Time[i] /= 2.0;
				}
				double num = Time[Time.Count - 1];
				double num2 = Time[Time.Count - 1];
				for (int j = 0; j < Time.Count - 1; j++)
				{
					list.Add(num + (num - Time[Time.Count - 2 - j]));
					num2 = list[j];
				}
				Time.AddRange(list);
				List<double> listClone2 = GetListClone(Percent);
				Percent.AddRange(listClone2);
				List<double> listClone3 = GetListClone(Vel);
				for (int k = 0; k < listClone3.Count; k++)
				{
					listClone3[k] = 0.0 - listClone3[k];
				}
				Vel.AddRange(listClone3);
			}
		}

		public void SetAutoBackEveryData(double position)
		{
			List<double> list = new List<double>();
			List<double> list2 = new List<double>();
			List<double> list3 = new List<double>();
			List<double> list4 = new List<double>();
			list.Add(0.0);
			list2.Add(0.0);
			list3.Add(0.0);
			list4.Add(0.0);
			for (int i = 1; i < Count - 1; i += 2)
			{
				list.Add(Position[i]);
				list.Add(Position[i + 1]);
				list.Add(Position[i]);
				list.Add(position);
				double num = (Time[i + 1] - Time[i - 1]) / 2.0;
				double num2 = (Time[i] - Time[i - 1]) / (Time[i + 1] - Time[i - 1]) * num;
				list2.Add(Time[i - 1] + num2);
				list2.Add(Time[i - 1] + num);
				list2.Add(Time[i - 1] + num + num2);
				list2.Add(Time[i - 1] + num + num);
				if (Mode == SingleSpeedTaskMode.PTS)
				{
					list3.Add(Percent[i]);
					list3.Add(Percent[i + 1]);
					list3.Add(Percent[i + 1]);
					list3.Add(Percent[i]);
				}
				else if (Mode == SingleSpeedTaskMode.PVT)
				{
					list4.Add(Vel[i]);
					list4.Add(Vel[i + 1]);
					list4.Add(0.0 - Vel[i + 1]);
					list4.Add(0.0 - Vel[i]);
				}
			}
			_position = list;
			_time = list2;
			_percent = list3;
			_vel = list4;
		}

		protected List<double> GetListClone(List<double> da)
		{
			List<double> list = new List<double>();
			for (int num = da.Count - 2; num >= 0; num--)
			{
				list.Add(da[num]);
			}
			return list;
		}
	}
}
