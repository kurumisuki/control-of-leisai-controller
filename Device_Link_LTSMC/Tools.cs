using System;
using System.Drawing;
using System.Reflection;

namespace Device_Link_LTSMC
{
	public static class Tools
	{
		public static ZeroMode GetZeroModeByString(string mode)
		{
			foreach (ZeroMode value in Enum.GetValues(typeof(ZeroMode)))
			{
				if (mode == value.ToString())
				{
					return value;
				}
			}
			return ZeroMode.一次回零加反找;
		}

		public static T GetEnumTypeByString<T>(string type)
		{
			foreach (T value in Enum.GetValues(typeof(T)))
			{
				if (type == value.ToString())
				{
					return value;
				}
			}
			throw new Exception("不能匹配正确类型");
		}


		public static string GetErroTypeTip(ErroType type)
		{
			Type typeFromHandle = typeof(ErroInfoClass);
			FieldInfo[] fields = typeFromHandle.GetFields();
			FieldInfo[] array = fields;
			foreach (FieldInfo fieldInfo in array)
			{
				if (fieldInfo.Name == type.ToString())
				{
					return fieldInfo.GetValue(null).ToString();
				}
			}
			return "";
		}

		public static T GetEnumTypeByType<T, V>(this V v)
		{
			foreach (T value in Enum.GetValues(typeof(T)))
			{
				if (Convert.ChangeType(value, typeof(V)).Equals(v))
				{
					return value;
				}
			}
			throw new Exception("未匹配!");
		}

		public static string GetErroTypeTipByErroCode(short code)
		{
			ErroType enumTypeByType = code.GetEnumTypeByType<ErroType, short>();
			return GetErroTypeTip(enumTypeByType);
		}
	}
}
