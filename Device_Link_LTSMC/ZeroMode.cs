namespace Device_Link_LTSMC
{
	public enum ZeroMode : ushort
	{
		/// <summary>
		/// 一次回零
		/// </summary>
		一次回零,
		一次回零加反找,
		二次回零,
		OTTZAEZ,
		EZTZ,
		OTTZAFFEZ,
		OPLS,
		OPAEZLS,
		EZLS,
		OPAFFEZLS
	}
}
