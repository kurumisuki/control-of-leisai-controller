namespace Device_Link_LTSMC
{
	public struct ZeroParameterGroup
	{
		public double Speed;

		public Direction Dirt;

		public ZeroParameterGroup(double Speed, Direction Dirt)
		{
			this.Speed = Speed;
			this.Dirt = Dirt;
		}
	}
}
